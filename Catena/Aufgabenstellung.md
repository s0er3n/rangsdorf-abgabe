Aufgabenstellung 

Beschreiben und interpretieren Sie die im Gelände gewonnen Daten zur Sedimentologie und Bodentypologie der Catena – einschließlich der Daten zur Standortaufnahme! Stellen Sie hierfür die Ergebnisse in geeigneter und lesbarer Form als Catena graphisch dar und interpretieren und diskutieren Sie die dargestellten Ergebnisse in Textform! Hinweise: - Achten Sie bei der graphischen Darstellung auf sinnvolle, erkennbare und konsistente graphische Ausdrucksformen und einen angemessenen Einsatz des Fachvokabulars (aus der KA5)! 

- Bitte kennzeichnen Sie die Lage der Catena in ihrer geomorphologischen Karte - Stellen Sie im Textteil die Vorgehensweise nur äußerst knapp dar (Was? Wo?)
- - Stellen Sie in Ihrer Diskussion Bezug zu 
  - 1) Ihren Ergebnissen aus der Übung zur geomorphologischen Kartierung, 
  - 2) bekannten Ergebnissen und 
  - 3) zur Landschaftsgenese; gehen Sie dabei in ausreichendem Maße auf die Literatur ein! Umfang des Textes: 1 bis max. 2 Seiten
<!-- ---
toc-title: Inhaltsverzeichnis
toc: true
bibliography: Quellen.bib
lang: de
linestretch: 1.5
papersize: a4
geometry:
- top=25mm
- left=25mm
- right=25mm
- bottom=30mm
--- -->
\newpage
# Einleitung

Am 05.10.2020 wurde im Rahmen des Geländepraktikums 2 Sondierungen entnommen.
Ziel dieser Übung ist es die Studenten, mit der Arbeit im Gelände vertraut zu machen und gleichzeitig Erkenntnisse zu der Landschaft und zum Boden zu gewinnen, indem mit Hilfe der Bodenkundlichen Kartieranleitung die Bodensequenz bestimmt werden soll.
Leider konnte dieses Jahr aufgrund der Coronasituation das Praktikum nur in gekürzter Form sowie unter strengen Hygiene- und Abstandsregeln statt finden, sodass die Anzahl der Bohrungen geringer als in vorherigen Jahren ausgefallen ist.


# Standort Beschreibung

<!-- ## Einordnung des Untersuchungsbereiches "Catena" in den Landschaftskontext -->

![Foto vom Hang von Sören Michaels](IMG_20201005_153748.jpg)


<!--
### Notizen

- (jungmoränen gebiet)
- Endmoränengebiet der Weichseleiszeit
- in ca. 45m ü. NN Höhe [@bose]
- Dünen periglazial entstanden
- Parabeldüne
- nacheiszeitliche viele Moore
- großer anthropogener Einfluss: Forst, NVA und Autobahn
- Kiefernwald
	- sandiger nährstoffarmer Boden
-->
Der Untersuchungsbereich für die Catena-Bohrungen befindet sich nördlich des Rangsdorfer Sees. 
Es handelt sich hier bei um einen Hang welcher gen Südwesten gerichtet ist.
Bei Betrachtung der Landschaft und des Bodens scheint es sich beim Untersuchungsgebiet um den Lee-Hang einer Parabeldüne zu handeln, die Periglazial aus den Sandern des Glogau-Baruther Urstromtals äolisch bewegt sein könnte.
Am Hang selbst befindet sich ein Nadelwald(Kiefer), welcher auf einen sandigen und Nährstoffarmen Boden hindeutet.
Am Hangfuß beginnt ein Laubwald.
Das Gebiet liegt auf der sogenannten Teltower Platte.
Es handelt sich dabei um eine jungmoränen Landschaft, die durch die Weichseleiszeit geprägt wurde[@bose].
Die Wege am Fuß des Hanges und parallel zu dem Untersuchungsgebiet deuten auf eine starke Anthropogene Nutzung hin[^1], des Weiteren befindet sich nördlich die Autobahn A10.
![Standort der Catenabohrungen](Standort.png)

## Witterungs- und Standortsbedingungen
<!-- 
- letzter Regen am Mittwoch dem 30.09.
- Temperaturen in den letzten 7 Tagen zwischen 7 und 21 Grad Celsius
- Kiefernwald
	- sandiger nährstoffarmer Boden
- in ca. 50m Höhe -->

Der letzte Regen im am Standort war am 30.09.2020 und die Temperaturen, der vorhergehenden sieben Tage, befanden sich zwischen 7 - 21 °C [@Wetter:online]. Der Untersuchungsstandort befindet sich in ca. 50m Höhe über NN[Abgelesen aus @Karte:online]. Der Kiefernwald deutet auf einen sauren Boden hin.

## Visualisierung mit eingetragenen Sondiererungs-/Bohrstandorten

![Bohrstandorte Bearbeiter: ](fa117eda-9460-41fb-bd08-05b0ae57033e.jfif)
<!-- # Beschreibung der jeweiligen Bohrkerne / der Sondierungen


## Visualisierung: Foto und Graphik  der Bohrkerne bzw. Bodenhorizontabfolgen mit Bennennung der Wichtigsten Bodenpararmeter (Korngröße, Bodenfarbe, Humusgehalt, Karbonat Anteil) für alle Sondierungen -->



### Bohrkern 1

![Foto Bohrkern 1 von Sören Michaels](IMG_20201005_140519.jpg)

![](Bohrkern1.png)

### Bohrkern 2

![Foto vom Bohrkern 2 von Sören Michaels](IMG_20201005_150515.jpg)


![](Bohrkern2.png)
<!-- ## Kurze schriftliche Beschreibung der jeweiligen Bohrkehransprachen und Nennung des Bodentyps -->
<!---
#### Notizen

Bohrkernansprache?

- Sand = Düne = Parabeldüne
- aus Sander im Süden
  - widerspruch zu Schütt
-->

<!-- - Leichte podsolierte Braunerde -->
\newpage
# Bohrkerne

Beide Bohrkerne sind sich sehr ähnlich. Diese Ähnlichkeit lässt sich durch die Nähe der beiden Bohrkerne zu einander erklären. Die Bohrkerne besitzen nur einen Höhen unterschied von maximal 1 m, sodass man keine wirkliche Toposequenz bekommt. 


## Bohrkern 1
| Infos                   |                                   |
| :---------------------- | :-------------------------------: |
| Projekt-Nr              |               MoN2                |
| Profil-Nr               |                 1                 |
| Bearbeiter              |          Sören Michaels           |
| Datum                   |            05.10.2020             |
| Koordinaten in UTM 33U  |        R0390642 H57967728         |
| Höhe                    | 49 - 48.5 m (abgelesen aus Karte) |
| Neigung                 |                12%                |
| Aufschlussart           |               BP-K                |
| Reliefformtyp           |               Hang                |
| Exposition              |             Südwesten             |
| Nutzungsart             |               Forst               |
| Vegetation              |        Kiefer (Nadelwald)         |
| Witterung               |  WT3 (letzter Regen vor 5 Tagen)  |
| Anthropogene Veränderung |         Starker Einfluss          |
Table: Bohrkern 1 Informationen

|  NR   | Grenze in cm | Symbol |      Farbe       | Humusgehalt | oxidation | Bodenart | Carbonat |
| :---: | :----------: | :----: | :--------------: | :---------: | :-------: | :------: | :------: |
|   0   |    0 - 8     |   L    |        -         |      -      |     -     |    -     |    -     |
|   1   |    8 - 13    |   Ah   |   Schwarz-Grau   |     h5      |     -     |    Ss    |    c0    |
|   2   |   13  - 18   |   Ae   |       Grau       |   h1 - h2   |     -     |    Ss    |    c0    |
|   3   |    18 -39    |   Bv   |    Hellbraun     |     h0      |   (EO)    |    Ss    |    c0    |
|   4   |   39 - 61    |   Bv   |  helleres Braun  |     h0      |   (EO)    |    Ss    |    c0    |
|   5   |   61  - 75   |   Cv   | gelbliches Braun |     h0      |     -     |    Ss    |    c0    |
|   6   |    75 - ?    |   C    | helles Gelbbraun |     h0      |     -     |    Ss    |    c0    |

Table: Bohrkern 1 Horizonte 

Bohrkern 1 befindet sich UTM 33U R0390642 H57967728.
Er ist damit der Bohrkern, der am Hang weiter oben liegt.
 Die Hangneigung beträgt 12 % also handelt es sich um einen mittel geneigten Hang (@eckelmann2006) die Exposition ist gen Südwesten. 
Das letzte Niederschlagsereignis hat 5 Tage vor der Bohrung in Form von Regen statt gefunden. 
Das Gebiet der Bohrung wird forstwirtschaftlich genutzt. 
Die anwesende Vegetation ist dem entsprechend Nadelwald in Form von Kiefern. 
Der anthropogene Einfluss scheint stark zu sein( siehe Standortbeschreibung )

Die Litterauflage hat eine Höhe von 8 cm über dem Humusreichen Ah-Horizont.
Der Ah-Horizont ist Schwarz-Grau mit einem Humusgehalt von 8 bis > 15 Masseprozent (Abgelesen aus @eckelmann2006).
Wie die weiteren Horizonte wurde die Bodenart zur Gruppe der Reinsande (Abgelesen aus @eckelmann2006)zugeordnet.
Mit Hilfe von Salzsäure 10% wurde dieser und alle weiteren Horizonte als cabonatfrei bestimmt.
Der Horizont besitzt eine Höhe von 5 cm.

Auf den Ah-Horizont folgt ein weiterer A-Horizont und zwar ein Ae-Horizont ein sogenannter Eluvialhorizont, dieser ist ein Hinweis auf Podsolierung.(@blumeSchefferSchachtschabelLehrbuch2010). 
Der Horizont besitzt eine gräuliche Farbe und der Humusgehalt liegt zwischen schwach und sehr schwach humos (@eckelmann2006).
Bodenart und Carbonatgehalt ist wie beschrieben Reinsand und Cabonatfrei.
Der Lesbarkeit wegen wird dies bei den weiteren Horizonten nicht mehr erwähnt.
Der Horizont ist 5 cm hoch.
Auf den  Ae-Horizont folgt ein großer Bv-Horizont.
In diesem scheint Eisenoxidation und/oder Mineralneubildung statt zu finden.
Dieser Schluss lässt sich aus der hellbraunen Farbe des Horizonts ziehen.
Dieser Prozess heißt Verbraung oder Verlehmung und deutet auf den Bodentyp Braunerde hin (@blumeSchefferSchachtschabelLehrbuch2010). 
Ob wirklich eine Verbraunung und Verlehmung stattgefunden hat lässt sich aber erst nach einer Untersuchung im Labor zeigen. 
Der Horizont ist Humusfrei und hat eine Höhe von 43 cm.
Als nächstes kommt der Cv-Horizont dieser liegt in 61 cm unter dem Boden. Er ist gelblich Braun und ist ebenfalls Humusfrei. Er ist schwach verwittert (@eckelmann2006).
Auf den Cv-Horizont folgt das Ausgangsmaterial, also der C-Horizont in einer Tiefe von 75 cm mit einer hellen gelbbraunen Farbe. 
Dieser ist ebenfalls Hummsfrei. Dieser Horizont ist der letzte unserer Bohrung. 

## Bohrkern 2

| Infos                   |                                                |
| :---------------------- | :--------------------------------------------: |
| Projekt-Nr              |                      MoN2                      |
| Profil-Nr               |                       2                        |
| Bearbeiter              |                 Sören Michaels                 |
| Datum                   |                   05.10.2020                   |
| Koordinaten in UTM 33U  |               R0309628 H5796716                |
| Höhe                    |           48 m (abgelesen aus Karte)           |
| Neigung                 |                      10%                       |
| Aufschlussart           |                      BP-K                      |
| Reliefformtyp           |                    Hangfuß                     |
| Exposition              |                   Südwesten                    |
| Nutzungsart             |                     Forst                      |
| Vegetation              |    Kiefer (Nadelwald) Übergang zu Laubforst    |
| Witterung               |        WT3 (letzter Regen vor 5 Tagen)         |
| Anthropogene Veränderung | Starker Einfluss direkt neben ehmaliger Straße |

Table: Bohrkern 2 Informationen

|  NR   | Grenzen in cm | Symbol |         Farbe          | Humusgehalt | oxidation | Bodenart | Carbonat |
| :---: | :-----------: | :----: | :--------------------: | :---------: | :-------: | :------: | :------: |
|   1   |    0 - 12     |   Ah   | dunkelbraun   gräulich |     h5      |     -     |    Ss    |    c0    |
|   2   |   12  - 41    |   Bv   |   hellbraun gräulich   |     h0      |     -     |    Ss    |    c0    |
|   3   |    41 - 62    |   Bv   |       Hellbraun        |     h0      |     -     |    Ss    |    c0    |
|   4   |    62 - 73    |   Cv   |   helleres Hellbraun   |     h0      |     -     |    Ss    |    c0    |
|   5   |    73 - ?     |   C    |       Hellbraun        |     h0      |     -     |    Ss    |    c0    |

Table: Bohrkern 2 Horizonte

Im Gegensatz zum Bohrkern 1 ist der zweite Bohrkern am Hang ungefähr einen Meter tiefer.
Die Steigung beträgt nur noch 10%.
Die restlichen Metadaten sind gleich bis auf, dass sich der Bohrkern etwas näher an einem alten Weg oder Straße befindet.
Wie auch in Bohrkern 1 ist dieser Bohrkern komplett carbonatfrei und die Bodenart ist Reinsand.
Der Ah-Horizont ist als dunkelbraun gräulich eingestuft worden er besitzt einen Humusgehalt von 8 - 15 Masseprozent. Er ist 12 cm hoch.
Der Nächste Horizont ist ein Bv-Horizont. Im Gegensatz zum ersten Bohrkern 1 folgt hier also kein Ae-Horizont, da aber in diesem Bohrkern der Ah-Horizont auch etwas gräulich ist könnt hier auch eine Podsolierung stattgefunden haben, die aber etwas weniger ausgeprägt ist und deswegen nicht im Geländer erkannt wurde.
Der Bv-Horizont ist 50 cm hoch. 
Er ist hellbraun gräulich.
Der grau Schimmer lässt aber mit der Tiefe ab.
Humusgehalt findet sich hier nicht mehr und auch nicht mehr in den tieferen Horizonten.
Wie auch beim Bohrkern 1 kann man hier von einer Verlehmung und Verbraunung ausgehen.
Dies ist ein Indiz, dass es sich hier um Braunerde handelt.
Auf den Bv-Horizont folgt ein 11 cm tiefer Cv-Horizont. Farblich ist er etwas heller als das Hellbraun des Bv-Horizontes. Der Humusgehalt liegt, wie zuvor erwähnt bei Null.
Auf den Cv-Horizont folgt der hellbraune C-Horizont der in einer Tiefe von 73 cm beginnt und unsere Bohrung beendet.

Beide Bohrkerne sind im Gelände als podsolierte Braunerde klassfziert worden.
Besondere Indizien für diesen Befund waren zum einen die Verbraunung, die besonders im Bv-Horizont zu sehen waren. 
Die Färbung ins rötliche entsteht durch die Verbraunung.
Das bedeutet durch chemische Verwitterung werden Eisenminerale im Boden freigesetzt, welche dann unter dem Einfluss von Wasser und Sauerstoff oxidieren, dadurch entsteht die bräunliche Farbe im Bv-Horizont[@blumeSchefferSchachtschabelLehrbuch2010].
Podsolierung ist besonders beim Bohrkern 2 zu sehen. 
Erkennbar an dem Eluvialhorizont.
Der Eluvialhorizont ist grau, da hier durch Sickerwasser die Minerale aus dem Horizont ausgewaschen wurden.
Dies passiert besonders, wenn die Humusmasse aus Nadelstreu besteht.
Wie in der Standortbeschreibung erwähnt handelt es sich hier bei dem Untersuchungsgebiet um einen Nadelwald. 
Der Nadelstreu ist sauer und begünstigt das auswaschen von Mineralen [@blumeSchefferSchachtschabelLehrbuch2010].
Da wir leider im Gelände nicht den Ph-Wert messen konnten, kann das hier nicht bestätigt werden.
Genauere Erkenntnisse bekommen erlangen wir nur durch die Auswertung der Proben im Labor.


 

# Diskussion der Ergebnisse im Gesamtkontext der Catena

Allgemein kann man bei der Untersuchung nicht von einer richtigen Catena-Untersuchung ausgehen. 
Aufgrund der erschwerten Bedingungen konnten nicht genug Bohrungen gemacht werden, um eine richtige Toposequenz zu erhalten.
Trotzdem lassen sich ein paar Erkenntnisse machen.
Wie in @bose beschrieben kann man hier die im Spätglazial auf gewehten Dünen erkennen.
Das es sich hier bei um eine Parabeldüne handelt erkennt man erst, wenn die weitere Umgebung betrachtet wird.
Braunerde geht im gemäßigten-humiden Klima aus Rankern, Regosolen oder Pararendzinen hervor [@blumeSchefferSchachtschabelLehrbuch2010].
In diesem Fall ging der Boden vermutlich aus einem Regosol hervor, dies würde die These, dass es sich beim Untersuchungsgebiet um eine Parabeldüne handelt stützen.
Ein Regosol besteht aus Lockersediment [@blumeSchefferSchachtschabelLehrbuch2010], wie zum Beispiel Sand, welcher äolisch dort abgelagert sein könnte.




<!-- ## Kurzer Vergleich der räumlichen Bodenabfolgen der gewonnen Bohrkernergebnisse

## Ableitung von sedimentologischen und pedogenen Prozesse entlang der Catenaabfolge

## Visualisierung: Anfertigung eines maßstabgetren Querprofils entlang der Catena unter Einzeichnung der Sondierungspunkte

# Kurze Zusammenfassung

## der wesentlichen sedimentologischen und pedogenen Befunde aus der Catenaabfolge in dem landschaftlichen Kontext -->

\pagebreak

# Literatur

[^1]: Forstwirtschaft und anderweitige Nutzung
---
include-after: \includepdf[pages=-]{GSGK_RECHNUNG.pdf} 
...

<!-- ![image-20201011235526130](image-20201011235526130.png) -->

\newpage

# Einleitung

Im Rahmen des Geländepraktikums wurde mit den Studenten einen Gewässerstrukturgütekatierung durchgeführt.
Leider konnte dieses Jahr aufgrund der Coronasituation das Praktikum nur in gekürzter Form sowie unter strengen Hygiene- und Abstandsregeln statt finden, sodass die Anzahl der kartierten Abschnitten geringer als in vorherigen Jahren ausgefallen ist.  

**Wasserhaushaltsgesetz**  

> Zweck dieses Gesetzes ist es, durch eine nachhaltige Gewässerbewirtschaftung die Gewässer als Bestandteil des Naturhaushalts, als Lebensgrundlage des Menschen, als Lebensraum für Tiere und Pflanzen sowie als nutzbares Gut zu schützen.

Das Verfahren der Gewässerstrukturgütekartierung ist eine Grundlage für Planungen und Entscheidungen [@lawa].
Es dient zur Erfassung/Dokumentation, Formulierung von Strukturgütezielen, Bewertung und Effizienznachweis von Maßnahmen in Gewässern [@lawa].

Seit den 70er Jahren gibt ist die Gewässergütekartierung in der Bundesrepublik Deutschland als ein Instrument des Gewässerschutzes[@StrukturguteFliessgewassernGrundlagen].
Bundesweit wurde dadurch die Methodik zur Messung der Verunreinigungen von Flüssen standardisiert[@StrukturguteFliessgewassernGrundlagen].
Die Darstellung der Güteklassen wurden auf einer Farbskala dargestellt.
Ende der 80er wurde dann viel Kritik an der Farbskala laut. 
Die Karte die durch die Farbskala entstanden sind seien viel zu grün und die eigentlichen Probleme sind nicht erkennbar[@StrukturguteFliessgewassernGrundlagen].
Um nun auch hinsichtlich Gewässerstruktur Zustandsverbesserungen auf den Weg zu bringen hat die Landesarbeitsgemeinschaft Wasser die Gewässerstrukturgütekartierung entwickelt [@lawa].
Diese ganzheitliche Betrachtung entspricht auch der EU-Wasserrahmenrichtlinie[@lawa].


# Material und Methoden

Bei der Gewässerstrukturgütekartierung ist wie folgt vorgegangen worden.
Als Material ist laut @lawa eine feste Unterlage, wie ein Klemmbrett im Zusammenhang mit einen wasserfest schreibenden Stift zu empfehlen.
Außerdem ist ein Fluchtstab oder ähnliches Gerät hilfreich.
Außerdem sind natürlich die Erhebungsbögen mitzuführen sowie eventuell auch ein Berechtigungsausweis.
Zu erst wurde mit Hilfe eines GPS-Gerätes die zu kartierenden Abschnitte markiert. 
Mit Abbildungen und Texten aus @lawa wurden darauf hin die Bewertungsbögen ausgefüllt.
Die wesentlichen Kriterien hier bei sind[@lawa]:
- unnatürliche Formveränderungen
- unnatürlicher Strukturverlust
- anthropogene Schadstruktur
Es wurden Einzelparameter, wie zum Beispiel Laufkrümmung, Querbänke und Substrattyp bestimmt.
Die verschiedenen Einzelparameter sind anhand der Beschreibung und Abbildungen im @lawa gut bestimmbar.
Wichtig ist hier bei, dass sich der komplette Abschnitt betrachtet wird.
Aus den Einzelparametern werden dann die Hauptparameter berechnet.
Da die Bewertung indexbasiert erfolgt werden die Indizes, die zu einem Hauptparameter gehören zusammen gerechnet und gemittelt werden. Dieser Mittelwert ist die Güteklasse des Hauptparameters. Hauptparameter sind zum Beispiel Laufentwicklung, Längsprofil und Querprofil  [@lawa]. 
Mehrere Hauptparameter ergeben einen Bereich, so ergeben Querprofil und Uferstruktur zusammen den Bereich Ufer.
Aus allen Bereichen (Sohle, Ufer, Land) ergibt sich die Gesamtbewertung für den Abschnitt[@lawa]. 
Die funktionalen Einheiten sind zur Überprüfung der Einzelparameter und sollten nur höchstens eine Klasse entfernt liegen von den Hauptparametern [@lawa].
Hat man für alle Abschnitte des Flusses eine Gesamtbewertung kann dieser nun klassifiziert werden [@lawa]. 

![Darstellung des Verfahrens aus @lawa](Verfahren.png)

## Fehlerdiskussion 

Die größte Fehlerquelle sind wahrscheinlich im Rahmen des Geländepraktikums die Studenten selbst.
Da die Studenten nur eine kurze Einweisung bekommen haben und in den meisten Fällen diese Kartierung eine ihrer ersten Gelände und Kartierungserfahrungen darstellt ist davon auszugehen, dass ihnen auf Grund dieser mangelnden Erfahrung Fehler unterlaufen sein könnten.
So könnten zum Beispiel die Laufkrümmung, Sohle und so weiter falsch eingeschätzt worden sein, da sich die Studenten verlesen haben könnten und oder sie sich auf Grund mangelnder Erfahrung vertan haben.
Des Weiteren könnten bei der Rechnung Verständnis und Rechenfehler auf getreten sein.




# Ergebnisse

![Abschnitt 1 Glasowbach. Foto von Amira Kunkel](abschnitt1.jpeg)

![Karte von den Abschnitten. Dunkelgrün ist die Güteklasse 3 also ein mäßigveränderter Abschnitt.](karte_GSGK.png)

## Abschnitt 1

Hauptparameter  
**Laufentwicklung**: 3.25  
**Längsprofil**: 7  
**Querprofil**: 2.25  
**Sohlenstruktur**: 3  
**Uferstruktur**: 3  
**Gewässerumfeld**: 1.25  
**Güteklasse** : 3.286  

## Abschnitt 2

Hauptparameter  
**Laufentwicklung**: 4.33     
**Längsprofil**: 7  
**Querprofil**: 2.25  
**Sohlenstruktur**: 3  
**Uferstruktur**: 2.5  
**Gewässerumfeld**: 1.25  
**Güteklasse** : 3.388  



![Die 2 Abschnitte in dunkel Grün und in Rot der Abschnitt des @GeoportalBrandenburgDetailansichtdienst und rechts die Daten des Geoportals](Differenz.png)


Die von uns kartierten Abschnitte bekommen bei unserer Gewässerstrukturgütekartierung die Güteklasse 3. 
Laut dem @LandschaftsrahmenplanLandkreisTeltowFlaming ist die Chemische Gewässergüte übermäßig verschmutzt.
Zur biologischen Gewässergüte gibt es im Landschaftsrahmenplan keine Angaben. 
Bei der Überprüfung von den von kartierten Ergebnissen sind Differenzen mit dem Datensatz @GeoportalBrandenburgDetailansichtdienst anzumerken.
Für den Abschnitt gibt der Datensatz an, dass es sich um den Zülowkanal handelt und die Strukturgüte bei Güteklasse 6 liegt.
Die Güteklasse widerspricht unseren Ergebnissen und weicht auch von der Güte aus ab @LandschaftsrahmenplanLandkreisTeltowFlaming. 
Hierbei könnte es sich um einen Fehler im Datensatz handeln.
Der Zülowkanal ist eindeutig nicht das kartierte Gewässer und die Güteklasse 6 entspricht auch nicht unserer Kartierung. 
Bei den Daten aus dem Geoportal könnte es sich um ein Parallel verlaufendes Gewässer handeln.  

\newpage

# Literatur
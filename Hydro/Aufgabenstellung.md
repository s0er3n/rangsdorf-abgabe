## Achtung – wichtige Hinweise:

- Min. 5 Quellenangaben 
- Min. eine Abbildung zur Messstelle sowie verwendeten Methode und diese auch im Text in Bezug bringen 
- Eine kleine Karte vom Gebiet in dem gemessen wurde inkl. eingezeichnetem Messstandort 
- Keine handschriftlichen Unterlagen einreichen, alles nur sauber digital oder digitalisiert anfertigen
- Auf die Lesbarkeit und Beschriftung von Abbildungen und Tabellen achten - Ergebnisse sind erst korrekt darzustellen und dann sinnvoll aufzurunden, Bsp.: 0,251325 m³/s ≈ 0,251 m³/s Wir wenden zur Berechnung das Querschnittsmittenverfahren an! 

## Aufgabe: 

1.) Ermitteln Sie aus den im Gelände, mit unterschiedlichen Methoden, gewonnenen Messdaten den Durchfluss für den jeweiligen Messpunkt und den Gesamtabfluss des Glasowbach-Einzugsgebiets oberhalb des Messstandortes (Bahnhofstraße)! 

2.) Stellen Sie Ihre Ergebnisse auf geeignete Art und Weise graphisch und in Textform dar! 

- Hinweise: 
- Achten Sie auf eine strukturierte Darstellung! Die Gliederung des Kapitels sollte sich an den folgenden Grobpunkten orientieren: 
<!---
# Notizen

Glasowbach

Die Daten für Luckenwalde aus dem Reader können auch für Rangsdorf genommen werden

Soll BER MIT entwässern

Wir sprechen nicht über den gesamt Abfluss sondern nur über einen Teil

Kriterien

Zugänglich

Wasser

Es sollte keinen Wasserstands Wechsel geben

Mess Punkt

Raue Gewässersohle

Schwer zu messen

Formel

Qm^3 pro Sekunde

A m^2

V m/s

Q = A * v

A = breite * tiefe

Breite 65

Tiefe 10

0.065m^2

Ergebnis

0,021m^3

Korrektur Faktor Mal 0.75

ADC Messgerät

2 Sensoren

Einen sendet Signal der andere empfängt.

Laufzeit wird gemessen

Snr

Signal Rausch Verhältnis

Wir machen eine einpunkt Messung
-->
\newpage

# Einleitung

Im Rahmen des Gelände Praktikums in Rangsdorf, wurde mit den Studierenden eine Durchflussmessung am Glasowbach durchgeführt. 
Ziel der Übung ist es nicht neue wissenschaftliche Erkenntnisse zu erlangen, sondern verschiedene Methoden kennen zu lernen.
Wichtig dabei war besonders, dass die Studenten ihre Ergebnisse kritisch hinterfragen und nachvollziehen können.

Der Glasowbach ist der Teltowplatte zugehörig [@landkreisteltowflamingRangsdorferSeeRegeln2020].
Teltowplatte ist eine schwach wellige Grundmoränenplatte [@landkreisteltowflamingRangsdorferSeeRegeln2020].
Außerdem hat der Glasowbach hat eine wichtige Funktion als Biotopverbundsystem zwischen den Naturschutzgebieten "Rangsdorfer See", "Blankenfelder See" und "Torfbusch" [@landkreisteltowflamingRangsdorferSeeRegeln2020].
Der Glasowbach ist besonders interessant, da geplant ist das Niederschlagswasser des Flughafens BER über den Glasowbach in den Rangsdorfer See zuleiten.
Das Ziel ist es den Rangsdorfer See mit dem Niederschlagswasser zu stabilisieren [@rocherMitteilungBurgermeistersGemeinde2019].
Geplant ist eine Verringerung der Phosphorbelastung im See sowie weitere ökologische und chemische Verbesserungen am See [@rocherMitteilungBurgermeistersGemeinde2019].




![Glasowbach](IMG_20201006_132922_1.jpg)


Der Glasowbach liegt in der Nähe des Rangsdorfers See.
Er verbindet die Naturschutzgebiete und Biotope "Rangsdorfer See", "Blankenfelder See" und "Torfbusch" [@landkreisteltowflamingRangsdorferSeeRegeln2020].

![Standort der Messstelle](Standortkarte.png)

# Messung I

| **Gewässer/Pegel**      | Glasowbach                |
| ----------------------- | ------------------------- |
| **Rechts-/Hochwert**    | N52° 19.7276 O13° 42.6445 |
| **Datum**               | 6.10.2020                 |
| **Uhrzeit**             | 13:00                     |
| **Bearbeiter**          | Sören Michaels            |
| **Wasserstand**         |                           |
| **(Pegellatte)**        |                           |
| **Wasserstand**         |                           |
| **(Drucksensor)**       |                           |
| **Messgerät**           | Treibkörpermessung        |
| **Kennziffer**          |                           |
| **Flügel/Schaufel**     |                           |
| **Messdauer pro Punkt** | 50s                       |

Table: Informationen Messung 1

> **Rechnung**  
> d = 2 Meter  
> w = 0,65 Meter   
> h = 0,10 Meter  
> A = 0,065 Meter<sup>2</sup>  
>  
> t<sub>1</sub> = 6,67 Sekunden  
> t<sub>2</sub> = 5,33 Sekunden  
> t<sub>3</sub> = 6,36 Sekunden  
> t<sub>mean</sub> = 6,12 Sekunden  
>  
> v = d / t<sub>mean</sub>  
> v = 2 Meter / 6,12 Sekunden  
> v = 0,3268 Meter/ Sekunde  
>  
> Q = A * v   
> Q = 0,065 Meter<sup>2</sup> * 0,3268 Meter/ Sekunde   
> Q = 0,021 Meter<sup>3</sup> / Sekunde  
> Q = 21 Liter / Sekunde  
>  
> Korrekturfaktor = 0.75 [@maniakHydrologieUndWasserwirtschaft2016] korrigiert die Rauhigkeit des Flussbettes
> Q<sub>k</sub> = Q * Korrekturfaktor  
> Q<sub>k</sub> = 21 Liter / Sekunde * 0.75  
> Q<sub>k</sub> = 15,75 Liter / Sekunde  

## Vorgehensweise

Bei der ersten Messung haben wir eine Treibkörpermessung durchgeführt.
Die Treibkörpermessung ist ein Schätzverfahren, um die ungefähre Fließgeschwindigkeit zu schätzen und oder den Gesamtabfluss.
Zuerst wird die ungefähre Breite und Tiefe des Gewässers geschätzt.
Nun wird ein Treibkörper, wie z.B. ein Ast oder ähnlich gesucht.
Der Treibkörper sollte ungehindert durch das Wasser treiben können.
Deswegen sollte die Messstelle von Hindernissen befreit werden.
Außerdem sollte der Treibkörper zur Hälfte ins Wasser eintauchen.
Um die Fließgeschwindigkeit zu "messen" wird jetzt ein Abschnitt vom Gewässer genommen, in dem der Treibkörper treiben kann.
Der Treibkörper wird möglichst mittig und ohne Schwung oder Energie vor dem Abschnitt ins Wasser gelassen. 
Dies geschieht vor dem Abschnitt, da der Treibkörper erst auf die Fließgeschwindigkeit kommen muss.
Ab dem Eintreten des Treibkörper in den Abschnitt wird die Zeit bis zum verlassen des Bereiches gestoppt.
Wird die Geschwindigkeit in Sekunde ausgerechnet er gibt das die geschätzte Fließgeschwindigkeit.
Aus der Fließgeschwindigkeit kann der Gesamtabfluss approximiert werden mit der allgemeinen Formel Q = A * v.
Die Rechnung wird nun noch mit einer Konstante angepasst, um ein verbessertes Ergebnis zu erlangen.
Diese Methode ist nur eine Schätzmethode und kann eigentlich nur durch Zufall wirklich genau sein.
Die Fließgeschwindigkeiten sind innerhalb des Gewässers sehr unterschiedlich.
Der Treibkörper nimmt allerdings nur einen Weg, in der Mitte der Oberfläche des Gewässers.
Außerdem wird bei der Methode nicht das Querprofil des Gewässers genau berechnet, sodass auch hier ein Fehlerfaktor vorliegt.
Es ist also nicht davon auszugehen, dass diese Methode genau ist.
Sie hat aber trotzdem einen Wert.
Sie bietet sich durch ihre Einfachheit und geringen Material sowie Zeitaufwand an, um schnell im Gelände Erkenntnisse zu gewinnen.
Für genaue Ergebnisse sollte man aber jedoch zu anderen Methoden greifen.


# Messung II 
| **Gewässer/Pegel**      | Glasowbach                |
| ----------------------- | ------------------------- |
| **Rechts-/Hochwert**    | N52° 19.7276 O13° 42.6445 |
| **Datum**               | 6.10.2020                 |
| **Uhrzeit**             | 14:22                     |
| **Bearbeiter**          | Sören Micheals            |
| **Wasserstand**         |                           |
| **(Pegellatte)**        |                           |
| **Wasserstand**         |                           |
| **(Drucksensor)**       |                           |
| **Messgerät**           | ADC VON OTT               |
| **Kennziffer**          |                           |
| **Flügel/Schaufel**     |                           |
| **Messdauer pro Punkt** | 50s                       |
Table: Informationen Messung 2

## Vorgehensweise/Methoden

Bei dieser Messung wurde ein Akustischer Digitaler Strömungsmesser von der Firma OTT verwendet, er funktioniert wie folgt:
Im Sensorkopf des Akustischer Digitaler Strömungsmesser befinden sich zwei Ultraschallwandler.
Die Partikel im Wasser reflektieren Ultraschallsignale.
Die Wandler senden Ultraschallsignale aus und Empfangen das Echo von den Partikeln im Wasser.
Die Echosignale werden nun weiter verarbeitet zu einem Echomuster.
Mit kurzem zeitlichen Abstand wird immer wieder erneut ein ein solches Echomuster erstellt.
Über den zeitlichen Versatz kann nun die Fließgeschwindigkeit berechnet werden. 
Zusammen mit Querfläche des Gewässers kann nun eine Durchflussmessung durchgeführt werden [@otthydrometgmbhBedienungsanleitungOTTADC].

Aufgrund der zeitlichen Begrenzung, die durch den veränderten Ablauf entstanden sind, wurde im Gelände nur die Fließgeschwindigkeit in mittlerer Höhe gemessen.
Außerdem ist der Pegelstand so niedrig an der Messstelle, dass es fast gar nicht möglich ist an dieser Messstelle auf drei verschiedenen Höhen zu messen.
Die Messung wurde vom einen Ufer zum anderen durchgeführt.
Alle 23 cm wurde die Wassertiefe bestimmt und dann mithilfe des Akustischen Digitalen Strömungsmesser die Fließgeschwindigkeit gemessen.
Wichtig bei dieser Messung war es, dass die Person die vermisst nicht den Wasserfluss um das Messgerät stört oder hindert und so mit nicht die Messung verfälscht wird.





 | Nr. | Position zum linken Rand [cm] | Wassertiefe [cm] | v (gemessen)[m/s] | Abschnittsbreite [cm]  | Abschnittsfläche [cm²]  | Abschnittsfläche [m²]  | Q [m³/s]     |               |
|-----|-------------------------------|------------------|-------------------|------------------------|-------------------------|------------------------|--------------|---------------|
| 1   | 0                             | 1,50             | 0,00              | 11,5                   | 17,25                   | 0,01725                | 0            |               |
| 2   | 23                            | 2,50             | 0,00              | 23,00                  | 57,5                    | 0,0575                 | 0            |               |
| 3   | 46                            | 6,00             | 0,00              | 23,00                  | 138                     | 0,138                  | 0            |               |
| 4   | 69                            | 12,00            | 0,02              | 23,00                  | 276                     | 0,276                  | 0,00552      |               |
| 5   | 92                            | 10,00            | 0,01              | 23,00                  | 230                     | 0,23                   | 0,0023       |               |
| 6   | 115                           | 10,00            | 0,02              | 23,00                  | 230                     | 0,23                   | 0,0046       |               |
| 7   | 138                           | 12,00            | 0,09              | 23,00                  | 276                     | 0,276                  | 0,02484      |               |
| 8   | 161                           | 12,00            | 0,11              | 23,00                  | 276                     | 0,276                  | 0,03036      |               |
| 9   | 184                           | 8,00             | 0,02              | 23,00                  | 184                     | 0,184                  | 0,00368      |               |
| 10  | 207                           | 6,00             | 0,00              | 23,00                  | 138                     | 0,138                  | 0            |               |
| 11  | 230                           | 3,00             | 0,00              | 11,50                  | 34,5                    | 0,0345                 | 0            |               |
|     |                               |                  |                   |                        |                         |                        | Summe [m³/s] | Summe [Liter] |
|     |                               |                  |                   |                        |                         |                        | 0,0713       | 71,3          |

Table: Messung und Rechnung
<!-- # 4.) Gesamtergebnis (5%) Gesamtabfluss des Glasowbach-Einzugsgebietes oberhalb der Messstelle (Bahnhofstraße) 5.) Diskussion (Wie sind die Ergebnisse zu bewerten?) (25%)  -->


# Gesamtergebnis

Pro Sekunde liegt der Gesamtabfluss bei 71,3 Litern oberhalb der Messstelle zu unserem Messzeitpunkt, dies wurde mit Hilfe der ADC Messung berechnet.
Es zeigt sich eine große Differenz zur Treibkörpermessung, welche eine 15,75 Liter pro Sekunde. 

Dieser erhebliche Unterschied kommt unteranderem dadurch zu Stande, dass die Treibkörpermessung nur ein Schätzverfahren ist.
Die Dimensionen vom Bach wurden nur geschätzt und entsprechen nicht den realen Werten.
Außerdem ist das Verfahren in sich schon sehr unpräzise.
Die Fließgeschwindigkeit wird nur auf der Route gemessen die der Treibkörper hinter sich legt.
Das Ergebnis hängt stark von den Treibeigenschaften und der Geschwindigkeit auf der Route ab.
Es ist also sehr ungenau von einer Treibkörpermessung auf den realen Abfluss zuschließen.
Die Treibkörpermessung kann dennoch nützlich sein, um zum Beispiel passende Messgeräte für die richtige Messung auszuwählen.

Die Ergebnisse der ADC-Messung müssen aber auch kritisch hinterfragt werden, es können sich leicht Fehler einschleichen.
So können falsche Bedienung des Messgerätes sowie falsche Programmierung das Ergebnis verfälschen.
Außerdem kann bei der Messung im Wasser die Fließgeschwindigkeit durch zum Beispiel Bewegungen des Messers beeinflusst werden.
Es kann auch passieren das die Beine ungünstig das Messgerät an der Messung hindern, auch kann das Messgerät nicht richtig in Strömungsrichtung gedreht werden.

Bei der Messung sollte auch noch klar gestellt werden, dass es sich um einen repräsentativen Abfluss handelt.
Das heißt, dass nicht kurz vor der Messung viel extra Wasser in den Bach geflossen ist, welches zum Beispiel vorher gestaut wurde.

Die oben beschriebenen Erklärungen für Messschwankungen und Ungenauigkeiten gelten auch beim Vergleich mit den langjährigen Monatsmittelwerten am Pegel Jüterbog-Bürgermühle.
Gleichzeitig muss beim Vergleich auch noch bedacht werden, dass es sich um unterschiedliche große Einzugsgebiete handelt.
Der langjährige Monatsmittelwert am Pegel Jüterbog-Bürgermühle liegt bei 222 Liter pro Sekunde bei einem Einzugsgebiet von 107,2 km${^2}$. Dies entspricht einem Abfluss von 2,1 Liter pro Sekunde pro km${^2}$[@FachbeitraegeLUGVHeft].
Hingegen beträgt der Wert bei unserer **einmaligen** Messung bei einem Einzugsgebiet von 13,4 km${^2}$ 5,3 Liter pro Sekunde pro km${^2}$. Vergleich kann man diese Werte jedoch nicht.
Bei unserem Wert handelt es sich nicht um einen langjährigen Mittelwert und je nach Zeit, Niederschlag sowie Temperatur und den obengenannten Faktoren wird dieser Wert erheblich anders ausfallen.
Ins Besondere, wenn Regenwasser vom BER in den Glasowbach geleitet wird [@rocherMitteilungBurgermeistersGemeinde].



<!-- →Erörterung potentieller Messfehler

→Vor- und Nachteile der einzelnen Methoden

 →Vergleich der Ergebnisse der einzelnen Methoden 

→ Vergleich mit den langjährigen Monatsmittelwerten am Pegel Jüterbog-Bürgermühle unter Berücksichtigung der unterschiedlichen Einzugsgebietsgrößen und kurze Erörterung, inwieweit die langjährigen Mittelwerte mit aktuellen Messwerten vergleichbar sind oder nicht. 



15 % maximaler Abzug wegen Formfehlern Bei der Beschreibung der Methoden ist die aktuelle Literatur (siehe Literaturliste im Reader) einzubeziehen. Werden die Standardverfahren eingesetzt, genügt deren exakte Bezeichnung (Fachbegriff) und ein Verweis auf die entsprechenden Quellen. Abweichungen von den Standardverfahren sind jedoch exakt zu beschreiben. Gleiches gilt für alle Aspekte, die speziell für die jeweilige Messstelle gelten und nicht verallgemeinert werden können (z.B. Messintervalle, Profilbreiten etc.). Achtung: Es wird von Ihnen eine eigenständige Rechenleistung erwartet, d.h. die Abflusswerte dürfen nicht in Gruppen berechnet werden! -->

\newpage

# Literatur
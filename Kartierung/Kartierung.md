<!-- ![image-20201011235709053](image-20201011235709053.png) 
![image-20201011235721954](image-20201011235721954.png) -->
\newpage

# Einleitung

Im Rahmen des Geländepraktikums wurde eine Geomorphologische Kartierung nördlich des Rangsdorfer Sees durchgeführt.
Dieses Gebiet liegt ungefähr 15km südlich von Berlin.
Es befindet sich im Baruther Urstromtal [@zoller_physische_2017].
Nach einer Einführung unter Leitung von Prof. Dr. Brigitta Schütt und Dr. Wahib Sawan
wurden durch die Studenten mit Hilfe von GPS-Geräten, Zeichenutensilen und  einer Kopie einer Karte des Gebiets gemorphologische Formen kartiert.
Mit dem Ziel drei Teilkarten zu erstellen.

\newpage

# Teilkarte 1 Thematische Karte der Geomorphologischen Formen- und Prozessbereiche

![Teilkarte 1](Karte_alle_namen.png)

Werden zunächst nur die Prozessbereiche betrachtet, so erkennt man eindeutig, das im Westen der Karte sich äolische Ablagerungen befinden. Besonders deutlich wird dies bei den Kesselbergen.
Bei den Kesselbergen handelt es sich eventuell um ein Dünengebiet [@luethgensNeubewertungGeomorphologischenEntwicklung].
Diese Ablagerungen sind vermutlich aus den in der Weichseleiszeit sich gebildeten Sandern endstanden.
Es könnte sich aber auch um eine ein gestauchte Endmoräne handeln.
Bei dem nördlichen Teil der Karte handelt es sich vermutlich hauptsächlich um Grundmoränengebiet.
Im Osten gibt es eine Auffälligkeit.
Dort gibt es eine circa 100 Meter lange Erhöhung.
Hier bei könnte es sich um ein Os handeln, welcher durch Schmelzwasser unter dem Gletscher entstanden ist.
Weiter südlich vom Oser befindet sich eine rechteckige Einbuchtung von mehreren Metern.
Diese könnte wahrscheinlich anthropogen abgetragen worden sein.
Indizien sind dafür, die nahliegende Autobahn sowie die Steigung die am Rand dieser Einbuchtung sich befindet.
Vermutlich wurde hier Material abgetragen zum Bau der Autobahn.
Eine weiterer großer anthropogener Eingriff befindet sich relativ zentral in der Karte. 
Hier bei handelt es sich um eine Sandgrube.
Anhand der Wurzeln der Bäume kann man hier sehen, dass Sand um die Bäume herum abgetragen wurden.
Die Bäume sind also leicht erhöht und die Wurzeln deutlicher sichtbar.
Weitere Anthropogeneflächen sind die Wege.
Insgesamt kann auch davon ausgegangen werden, dass komplette Kartengebiet eigentlich anthropogen über prägt ist.
Durch die Monokulturen im Gebiet erkennt man, dass hier Forstwirtschaft betrieben wurde, außerdem gab es in der Nähe in Rangsdorf einen sowjetischen Militärstandort, sodass nicht ausgeschlossen werden kann, dass keine militärische Übungen durchgeführt wurden [@SowjetluftwaffeBis1994].

![Vertiefung in der Nähe der Autobahn](IMG_20201007_150554.jpg)

# Teilkarte 2 Lineare Reliefelemente

![Teilkarte 2](Teilkarte_2.png)

In der Teilkarte 2 sieht man die linearen Reliefelemente.
Im Kartiergebiet befinden sich Kuppen, Kanten, Erhöhungen, Schwemmfächer, Vollformen sowie aktive Erosion. 
Wie schon bei Teilkarte 1 angsprochen erkennt man auch hier das vermutete Os.
Im Gebiet der Kesselberge befinden sich viele Kuppen, Kanten und Erhöhungen sowie Schwämmfächer.


# Teilkarte 3 Hydrographie
![Teilkarte 3](Karte_Hydrographie_alle_namen.png)

Im Kartiergebiet konnten keine Hydrographie Merkmale festgestellt werden.
Auch mit Hilfe von Drittanbieter Karten wurden keine solche Merkmale gefunden.
Es ist also davon auszugehen, dass hier keine zu kartierenden Hydrographie Merkmale vorliegen.

\newpage

# Literatur


Aufgabenstellung:

Ermitteln Sie aus denen im Gelände mit Hilfe des Kartierblatts gewonnenen Daten den Bodentyp für die jeweilige Schürfgrube! Stellen Sie hierfür Ihre Ergebnisse auf geeignete Art und Weise sowohl graphisch als auch in Textform dar! Hinweise: ¾ Achten Sie auf einen strukturierten Aufbau ihres Berichts: 

1. Beschreibung 

2. Diskussion

3. Synthese

- Nutzen Sie aktiv die Erläuterungen aus der bodenkundlichen Kartieranleitung zu Deutung / Interpretation des Kartierblattes sowie weitere Informationen aus der Literatur!
-  Nehmen Sie Bezug zur Landschaftsgenese! 
- Verwenden Sie Fotos und Profilzeichnungen!
-  Achten Sie beim Einzeichnen der Horizontgrenzen auf die reale Wiedergabe entlang der Profilwand!
-  Überprüfen Sie die aufgenommenen Parameter auf Sinnhaftigkeit!
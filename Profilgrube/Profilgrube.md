
\newpage

# Einleitung


Am 06.10.2020 wurde im Rahmen des Geländepraktikums eine Profilgrube ausgehoben.
Ziel dieser Übung ist es die Studenten, mit der Arbeit im Gelände vertraut zu machen und gleichzeitig Erkenntnisse zu der Landschaft und zum Boden zu gewinnen, indem mit Hilfe der Bodenkundlichen Kartieranleitung der Bodentyp bestimmt werden soll.
Des weiteren wurden Bodenproben entnommen für die verschiedenen Horizonten, um eine genauere Analyse im Labor durchzuführen.
Leider konnte dieses Jahr aufgrund der Coronasituation das Praktikum nur in gekürzter Form sowie unter strengen Hygiene- und Abstandsregeln statt finden, sodass die Tiefe der Profilgrube kleiner als in vorherigen Jahren ausgefallen ist.


# Standortbeschreibung

<!-- - Einordnung in die Landschaft, räumlicher Kontext, Witterungs- und Standortbedingungen, Datum, Profilname, Koordinaten -->
Es handelt es sich hier bei um das Profil DIV-2 vom 06.10.2020 UTM 33U R390829,5 H5796805.
Das Profil ist Regen beeinflusst sowohl Nachts als auch am Tag der Aushebung gab es Niederschlag.
Neben dem Aushebungsgebiet befand sich einen Weg, so ist anthropogener Einfluss nicht auszuschließen, sondern wahrscheinlich.
Der Untersuchungsbereich für die Profilgrube befindet sich nördlich des Rangsdorfer Sees.
Um das Untersuchungsgebiet herum befindet sich ein Wald aus Kiefern und Birken, dieser Liegt in etwa 50 m. über NN. 
Das Gebiet liegt auf der sogenannten Teltower Platte.
Es handelt sich dabei um eine jungmoränen Landschaft, die durch die Weichseleiszeit geprägt wurde[@bose].


| **Projekt-Nr**              | DIV-2              |
| --------------------------- | ------------------ |
| **Profil-Nr**               | 1                  |
| **Bearbeiter**              | Sören Michaels     |
| **Datum**                   | 06.10.2020         |
| **Koordinaten in UTM 33U**  | R390829,5 H5796805 |
| **Höhe**                    | ca. 50             |
| **Neigung**                 | 0 %                |
| **Aufschlussart**           |                    |
| **Reliefformtyp**           |                    |
| **Exposition**              | Norden             |
| **Nutzungsart**             |                    |
| **Vegetation**              | Kiefer und Birke   |
| **Witterung**               | WT5                |
| **Anthropogene Veränderung** | neben einen Weg    |

Table: Profilinformationen

<!-- - Visualisierung: Karte mit Standort, Foto der Standortbedingungen  -->

# Profilbeschreibung 

![Werkzeug](IMG_20201006_084851.jpg)



![Darstellung Profil Grube (Bearbeiter: Louisa Wiegels)](profilgrube.png)

![Entnahme der Bodenproben](IMG_20201006_114134.jpg)


![Profilgrube Bild von Amira Kunkel](d0fd7638-81fa-48af-bf0b-434df554d6dd.jpg)

![Zeichnung des Bodens](Zeichnung.png)

| **Farbbestimmung** |           |      |                         |
| ------------------ | --------- | ---- | ----------------------- |
| **Horizont**       | **Farbe** |      |                         |
| organ. Auflage     | 10YR      | 3/2  | very dark greyish brown |
| 0 - 5              | 10YR      | 4/2  | dark greyish brown      |
| 10 - 15            | 10YR      | 5/2  | greyish brown           |
| 15 - 20            | 10YR      | 4/1  | dark grey               |
| 25 - 30            | 10YR      | 4/1  | dark grey               |
| 30 - 35            | 10YR      | 5/2  | greyish brown           |
| 40 - 45            | 10YR      | 5/2  | greyish brown           |
| 45 - 52            | 10YR      | 6/3  | pale brown              |
| 55 - 60            | 10YR      | 6/4  | light yellowish brown   |
| 65 - 70            | 10YR      | 6/4  | light yellowish brown   |
| 70 - 75            | 10YR      | 6/4  | light yellowish brown   |
| 80 - 85            | 10YR      | 6/6  | brownish yellow         |
| 90 - 95            | 10YR      | 6/6  | brownish yellow         |

Table: Farben der Proben

<!-- 
- Zusammenfassung der Bodeneigenschaften nach Aufnahmebogen (im Anhang hinzufügen incl. Probenliste): 
  - reine Beschreibung nach Horizonten gegliedert 
  - Visualisierung: Foto und Zeichnung des Bodenprofils -->

## Aeh-Horizont

Bei dem ersten Horizont handelt es sich um einen Aeh-Horizont er ist schwach humos, besteht aus sandigen Sand und ist carbonatfrei(Bezeichnungen aus @bodenBodenkundlicheKartieranleitungKA52006).
Er ist dunkelbraun bis grau gefärbt.
Aufgrund der begrenzten Zeit konnten wir nicht den PH-Wert messen und der Eigenschaften des Horizontes und aus dem Gesamtkontext erschließt sich das es sich bei diesen Horizont um einen Aeh-Horizont handelt. 
Dieser ist schwach podsoliert und durch Humusverlagerung gekennzeichnet.

## Bv-Horizont

Nach dem Aeh-Horizont kommt der Bv-Horizont.
Er ist hellbraun bis gräulich, außerdem ist er schwach humos mit sandigem Sand und carbonatfrei(Bezeichnungen aus @bodenBodenkundlicheKartieranleitungKA52006). 
Es ist ein durch Verwitterung verbraunter und verlehmter Boden.

## C-Horizont

Bei dem C-Horizont handelt es sich um das Ausgangsgestein des Bodens.
Er ist hell-gelblich Braun gefärbt und nicht humos außerdem ist er carbonatfrei(Bezeichnungen aus @bodenBodenkundlicheKartieranleitungKA52006).

# Fazit

<!-- 
- Zusammenfassende, horizontübergreifende und vergleichende Analyse der Bodeneigenschaften. 
- Ableitung von sedimentologischen und pedogenen Prozessen 
- Nennung und Begründung des wahrscheinlichsten Bodentyps 
- Visualisierung: Kennzeichnung der Prozesse im Bodenprofil (in 2.), evtl. graphische Aufbereitung der Zusammenhänge  -->

Bei dem Boden handelt es sich vermutlich um eine leicht podsolierte Braunerde. 
Aus dem leicht eluvialen A-Horizont lässt sich schließen, das hier im Oberboden Silikatzerstörung, Oxidlösung und Nährstoffabfuhr statt gefunden haben, welche durch saure Bedingungen ausgelöst wurden [@gebhardtGeographiePhysischeGeographie2020]. 
Da der Boden vermutlich im Spätglazial ungefähr vor 15000 bis 11700 Jahren entstanden ist, sind die Verwitterungsprozesse nicht voll ausgeprägt[@gebhardtGeographiePhysischeGeographie2020]. 
Des weiteren kann man im Oberboden, durch die Färbung innerhalb des Horizontes, die Verarmung und Anreicherung von Huminstoffen erkennen [@glaserBodenEndlicheRessource2010].
Im B-Horizont findet eine Verbraunung und Verlehmung statt.
Durch Verwitterung von Mineralen wird Eisen freigesetzt, welches unter Einfluss von Sauerstoff oxidiert [@semmelGrundzuegeBodengeographie1977].
Deutlich findet dieser Vorgang bei einem PH-Wert unter 7 statt[@semmelGrundzuegeBodengeographie1977], ob dies hier zu trifft müsste im Labor überprüft werden. 
Eine erhöhte Verbraunung geht oft mit einer Erhöhung des Tongehaltes einher, dies nennt man Verlehmung [@semmelGrundzuegeBodengeographie1977].
Da der Boden durch die weichseleiszeitlichen Sander eine deutliche Schluffkomponente enthält fällt hier in diesem Boden die eigentlich kräftig entwickelte Podsolierung leicht aus [@semmelGrundzuegeBodengeographie1977].

\newpage

# Literatur

<!-- # 4. Zusammenfassung (15%): 

- Einordnung der sedimentologischen und pedogenen Befunde in den landschaftsgeschichtlichen Kontext und die aktuelle Landnutzung -->
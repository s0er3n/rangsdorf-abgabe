import os
import time
import pathlib
import shutil
# from PyPDF2 import PdfFileMerger

folders =[ name for name in os.listdir(".") if os.path.isdir(os.path.join(".", name)) ]
start = os.getcwd()
# merger = PdfFileMerger()
for folder in folders:
    if not folder.startswith("."):
        os.chdir( start+"\\" +folder )

        os.system(f"pandoc .\{folder}.md --defaults ..\pdf.yaml")
        time.sleep(1)
        os.replace("output.pdf",folder+"_Sören_Michaels_5223508.pdf")
        shutil.move(folder+"_Sören_Michaels_5223508.pdf", start+ "\\.output")
        # merger.append(open( fr"C:\Users\soeren\Documents\rangsdorf-abgabe\{folder}\output.pdf", 'rb'))

# os.chdir(start)
# # with open("Geländepraktikum_Rangsdorf_Sören_Michaels_5223508.pdf", "wb") as f:
#     merger.write(f)